require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  def setup
    @user = users(:one)
    @course = @user.courses.build(hour: 10, name: "German")
  end
  
  test "course should be valid" do
    assert @course.valid?
  end
  
  test "user_id exists" do
    @course.user_id = nil
    assert_not @course.valid?
  end
  
  test "name exists" do
    @course.name = ""
    assert_not @course.valid?
  end
  
  test "hour exists" do
    @course.hour = nil
    assert_not @course.valid?
  end
  
  test "sessions to owe exists" do
    @course.sessions_to_owe = nil
    assert_not @course.valid?
  end
end
