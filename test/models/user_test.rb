require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users(:one)
  end
  
  test "user should be valid" do
    assert @user.valid?
  end
  
  test "email exists" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "email length needs to be appropriate" do
    @user.email = "p" * 250 + "@testmail.com"
    assert_not @user.valid?
  end
end
