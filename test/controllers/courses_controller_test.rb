require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  def setup
    @course = courses(:one)
  end
  
  test "should redirect to index when authenticated" do
    sign_in users(:one)
    get :index
    assert_response :success
  end
  
  test "should get new" do
    sign_in users(:one)
    get :new
    assert_response :success
  end
  
  test "should get edit" do
    sign_in users(:one)
    get :edit, id: @course
    assert_response :success
  end
  
  test "should show course" do
    sign_in users(:one)
    get :show, id: @course
    assert_response :success
  end
  
  test "should create course" do
    sign_in users(:one)
    assert_difference('Course.count') do
      post :create, course: {name: "German", hour: 6}
    end
    assert_redirected_to course_path(assigns(:course))
  end
  
  test "should update course" do
    sign_in users(:one)
    patch :update, id: @course, course: {name: "English", hour: 8}
    
    assert_response :success
  end
  
  test "should delete course" do
    sign_in users(:one)
    assert_difference('Course.count', -1) do
      delete :destroy, id: @course
    end
    
    assert_redirected_to courses_path
  end
end
