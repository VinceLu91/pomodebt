# Pomodebt Project

This is an attempt to create a web app from scratch. I have been working on building this application since November 2014. 

This application is intended to help students keep track of how many pomodoros that they have have yet to complete in a week for each course. If a student wants to make sure that he/she stays focused on studying a course, while making sure that they use their time well, this is for them.

Since this is a Rails application project, I've built a way for users to authenticate themselves using a gem called Devise, which enables developers to customize how users sign in to the system. When this happens, a user ActiveRecord model is created.

Besides creating users, I have also created a scaffolding called "courses", which contains ActiveRecord model, controller, and view for the Course resource. Each course contains data for how many pomodoros have yet to "owe" (how many a user has left to study) and how many are "paid" (number of pomodoros completed). For each user to determine how many pomodoros to study per course per week, he would need to fill in the number of hours that he is estimated to complete, and so the Course Controller would compute the number of pomodoros  for that week.

In addition, I have used the Keen IO analytics API as a way of white-labelling data for individual users. I used the API to create a bar chart to record how many pomodoros that a student has completed for each course. Fortunately, I have also applied filters so that each student/user can only see and track his/her data.

There are some things that I want to - and hope to be able to - build as a way of learning to build an application from the ground up. One of the things is being able to create a mailer that sends out email to users every week (through cron, or a similar tool) about their results from the last week of studying. Another feature that I want to build is the ability to accumulate the set number of pomodoros each week so the user would know how many sessions left that he/she has to owe. 

The reason for wanting to implement these features is because this way the user experience would have made more intuitive sense: the user may not always have to log on to the application to track the pomodoros for his courses. But by sending an email each week he would be reminded of how well he has been studying so far. Also, in the current version of the app, the user would have to manually set his weekly hours in order for the number of pomodoros to be calculated. By using cronjob, it would have simplified the workflow for the user, as he would already have known how many pomodoros he would need to study in the new week for each course, as well as how many pomodoros are completed.

To this day, I have learned much about web development and many things about Rails and Ruby. I can't say that I'm a total expert yet, but the skills and knowledge that I've been picking up along the way have lead to the current state of the application.

Here are some of the source codes that I've written that demonstrate the skills and insights that I have gained from building:
An ActionController for the Course Resource https://bitbucket.org/VinceLu91/pomodebt/src/c6aa3d8e5af60f4f2acba1b23a301d0b871f61c3/app/controllers/courses_controller.rb?at=master

An ActiveRecord for the Course Resource - this model contains some methods which act on the database value, and so are called by instance variables in the aforementioned ActionController
https://bitbucket.org/VinceLu91/pomodebt/src/c6aa3d8e5af60f4f2acba1b23a301d0b871f61c3/app/models/course.rb?at=master. 

This is the actual show view for the Course resource. It includes the use of adding a template partial for the countdown timer code. https://bitbucket.org/VinceLu91/pomodebt/src/c6aa3d8e5af60f4f2acba1b23a301d0b871f61c3/app/views/courses/show.html.erb?at=master

Update (May 22/2015): I have written some fairly robust unit tests to cover my existing implementations. The notion of good test coverage and test-driven coding has eluded me when I was learning Rails in the first place, given how intimidating testing can be in the beginning. At this point, I am using Minitest, and it's been serving me well.