Rails.application.routes.draw do
  resources :courses do
    member do
      patch 'update_pomodoro_count'
    end
  end

  devise_for :users
  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'

end
