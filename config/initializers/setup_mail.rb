ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :address              =>  'smtp.sendgrid.net',
  :port                 =>  '25', #'587',
  :authentication       =>  :plain,
  :user_name            =>  ENV['SENDGRID_USERNAME'], #'app31775594@heroku.com',
  :password             =>  ENV['SENDGRID_PASSWORD'], #'dcq5oyno',
  :domain               =>  ENV['SENDGRID_DOMAIN'], #'heroku.com',
  :enable_starttls_auto =>  true
}