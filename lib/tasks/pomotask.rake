namespace :pomotask do
  desc "reset the pomstoday attribute to 0 daily"
  task reset_pomstoday: :environment do
    
    courses = Course.all
    courses.each do |course|
      course.update_attribute(:pomstoday, 0)
    end
    puts "it works!"
  end

end
