class AddMinuteToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :minute, :integer
  end
end
