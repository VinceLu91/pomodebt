class AddScopedKeyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :scoped_key, :string
  end
end
