class RemoveMinuteAndSecondFromCourses < ActiveRecord::Migration
  def change
    remove_column :courses, :minute, :integer
    remove_column :courses, :second, :integer
  end
end
