class AddCourseIdToDailypoms < ActiveRecord::Migration
  def change
    add_column :dailypoms, :course_id, :integer
    add_index :dailypoms, :course_id
  end
end
