class AddSecondToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :second, :integer
  end
end
