class RemovePomscompletedFromCourses < ActiveRecord::Migration
  def change
    remove_column :courses, :pomscompleted, :integer
  end
end
