class ChangeNames < ActiveRecord::Migration
  def change
    rename_column :courses, :pomsleft, :sessions_to_owe
    rename_column :courses, :pomstoday, :sessions_calculated
  end
end
