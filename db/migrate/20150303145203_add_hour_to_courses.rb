class AddHourToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :hour, :integer
  end
end
