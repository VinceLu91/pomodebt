class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
    render text: "hello, world!"
  end
  
  def after_sign_in_path_for(resource)
    #if resource.sign_in_count == 1 # first time, login by confirmation
    if resource.subscribed != true
      courses_path #'/charges/new'
    else
      courses_path
    end
  end
end