class CoursesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    index_flash = ["Welcome back! Ready to owe your pomodoros this week?", "Simply click on \'Edit Course\' and add your new estimated number of hours to study for the week.", "If you haven't finished owing your sessions from last week, you need to owe them from this week's accumulated debt."]
    flash.now[:success] = index_flash.join("<br/>").html_safe
    @courses = current_user.courses.all
    respond_with(@courses)
  end

  def show
    show_flash = ["To properly keep track of your Pomodoros, please don't refresh it or the starting Pomodoro will go back to 1.", "You can choose to leave this session when you are done."]
    flash.now[:success] = show_flash.join("<br/>").html_safe
    respond_with(@course)
  end

  def new
    @course = current_user.courses.new
    respond_with(@course)
  end

  def edit
  end

  def create
    @course = current_user.courses.new(course_params)
    if @course.valid?
      @course.update_attribute(:sessions_to_owe, @course.get_sessions_to_owe)
    end
    respond_with(@course)
  end

  def update
    @course.update(course_params)
    if @course.valid?
      @course.update_attribute(:sessions_to_owe, @course.get_sessions_to_owe)
    end
    respond_with(@course)
  end

  def destroy
    Keen.delete(:pomsCount, filters: [{
      :property_name => 'course_name', :operator => 'eq', :property_value => @course.name
    }])  
    @course.destroy
    respond_with(@course)
  end
  
  def update_pomodoro_count
    @course = current_user.courses.find(params[:id])
    Keen.publish("pomsCount", { :course_id => @course.id, :course_name => @course.name, :user_id => @course.user_id })
    @course.decrement_sessions_to_owe
  end

  private
    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :hour)
    end
end
