class ChargesController < ApplicationController
  
  before_action :authenticate_user!
  
  # https://stripe.com/docs/checkout/guides/rails
  def new
  end

  def create
    # Get the credit card details submitted by the form
    token = params[:stripeToken]
    
    # Create a Customer
    customer = Stripe::Customer.create(
      :card => token,
      :plan => 'test_with_trial',
      :email => current_user.email
    )
    
    current_user.subscribed = true
    current_user.stripeid = customer.id
    current_user.save
    
    redirect_to courses_path, :notice => "Your subscription was setup!"
    
  end
  
  def destroy
    if current_user.subscribed == true
      customer = Stripe::Customer.retrieve(current_user.stripeid)
      customer.delete # delete whole customer/user anyway
    end
    if current_user.destroy
      flash.alert = "Your subscription and account has been cancelled successfully!"
      redirect_to root_path
    end
  end

end
