json.array!(@courses) do |course|
  json.extract! course, :id, :name, :pomscompleted, :pomsleft
  json.url course_url(course, format: :json)
end
