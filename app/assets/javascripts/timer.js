var timeInSecs;
var ticker;
var time, seconds;

function startTimer()
{
	//timeInSecs = secs;
	toReset();
	myStopFunction();
	ticker = setInterval("tick()",1000);   // every second
}

function tick()
{
	if(seconds > 0 || minutes > 0)
	{
		if(seconds == 0)
		{
			minutes = minutes - 1;
			seconds = 59;
		} else {
			seconds = seconds - 1;
		}
	}
	if(seconds < 10) seconds = "0" + seconds;
	$("#timer").val(minutes + ":" + seconds);
	
	// this is for times up
	if(seconds <=0 && minutes <= 0) {
		 myStopFunction();
		 new Ajax.Request('courses/boo', {
			 method: 'post'
		 });
		 return -1;
	}
}

function myStopFunction()
{
	clearInterval(ticker);
}

toReset = function()
{
	start_time = "00:05"
	time = start_time.split(":");
	minutes = parseInt(time[0]);
	seconds = parseInt(time[1]);
	if(seconds < 10) seconds = "0" + seconds;
	$("#timer").val(minutes + ":" + seconds);
}