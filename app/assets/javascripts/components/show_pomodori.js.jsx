var ShowPomodori = React.createClass({
  propTypes: {
    paid: React.PropTypes.number,
    owe: React.PropTypes.number
  },

  render: function() {
    return (
      <div>
        <div><strong>Number of Sessions Paid This Week:</strong> {this.props.paid}</div>
        <div><strong>Number of Sessions to Owe:</strong> {this.props.owe}</div>
      </div>
    );
  }
});
