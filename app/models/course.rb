class Course < ActiveRecord::Base
  belongs_to :user
  
  validates :user_id, presence: true
  validates :hour, presence: true
  validates :sessions_to_owe, presence: true
  validates :name, presence: true
  
  default_value_for :sessions_to_owe, 0
  
  def decrement_sessions_to_owe
    if self.sessions_to_owe > 0
      self.decrement!(:sessions_to_owe)
    end
  end
  
  def get_sessions_to_owe
    self.sessions_calculated = self.hour * 2
    self.sessions_to_owe += self.sessions_calculated
  end
end
