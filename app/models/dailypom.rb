class Dailypom < ActiveRecord::Base
  belongs_to :course
  validates :course_id, presence: true
  
  def self.create_daily
    Dailypom.create!
  end

  def self.daily_count(from = (Time.zone.now.to_date).beginning_of_day,to = Time.zone.now.to_date.end_of_day)
    hash = Dailypom.where(created_at: from..to).group('date(created_at)').count
    if hash[Date.today.to_s].blank?
      return 0
    else
      return hash[Date.today.to_s]
    end
  end

  def self.monthly_count(from = (7.days.ago).to_date,to = Time.zone.now.to_date.end_of_day)
    date_hash = {}
    ((from)..(to)).each{|date| date_hash[date.strftime("%F")] = 0 }
    hash1 = Dailypom.where(created_at: from..to).group('date(created_at)').count
    hash1.reverse_merge!(date_hash)
    hash2 = Hash[hash1.sort]
    hash2.values
  end

  def self.daily_axis(from = (7.days.ago).to_date,to = Time.zone.now.to_date.end_of_day)
    date_hash1 = {}
    ((from)..(to)).each{|date| date_hash1[date.strftime("%F")] = 0 }
    hash3 = Dailypom.where(created_at: from..to).group('date(created_at)').count
    hash3.reverse_merge!(date_hash1)
    hash4 = Hash[hash3.sort]
    hash4.keys
  end
end
