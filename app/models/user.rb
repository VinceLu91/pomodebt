class User < ActiveRecord::Base
  has_many :courses
  
  validates :email, presence: true, length: { maximum: 255 }
  
  after_initialize :init_scoped_key
  
  def init_scoped_key
    self.scoped_key ||= Keen::ScopedKey.new(ENV["KEEN_MASTER_KEY"], {
      "allowed_operations" => ["read"]
    }).encrypt!
  end
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
